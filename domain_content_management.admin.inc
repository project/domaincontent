<?php

/**
 * @file
 * Provides content management include functions.
 */

/**
 * Function for content management form.
 */
function domain_content_management_form($form, &$form_state) {
  $domains = domain_content_management_get_domain_options();
  $form['node_id'] = array(
    '#type' => 'textarea',
    '#title' => t('Node IDs'),
    '#required' => TRUE,
    '#description' => t('A comma-separated list of node IDs.'),
  );
  $form['domain'] = array(
    '#title' => t('Domain'),
    '#type' => 'select',
    '#size' => 3,
    '#options' => $domains,
    '#required' => TRUE,
    '#multiple' => TRUE,
    '#description' => t('Select which affiliates can access this content. If select "All domains", it will override all other domain specific settings.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validate the domain_content_management_form.
 */
function domain_content_management_form_validate($form, &$form_state) {
  // Validate node id.
  $node_ids = explode(",", $form_state['values']['node_id']);
  foreach ($node_ids as $nid) {
    if ($nid != '' && !is_numeric($nid)) {
      form_set_error('node_id', t('Node id must be a number.'));
    }
  }
}

/**
 * Submit function for  the domain_content_management_form.
 */
function domain_content_management_form_submit($form, &$form_state) {
  $node_ids = explode(",", $form_state['values']['node_id']);
  $node_ids = array_unique($node_ids);
  // On form submit iterate the array and delete the pre-entered values
  // for the nodes, along with that enter the new settings for the
  // corresponding node.
  foreach ($node_ids as $nid) {
    $node = node_load($nid);
    $node->domains = NULL;
  }
  $count = 0;
  foreach ($node_ids as $nid) {
    $count++;
    // Check the node id is exist.
    $node_id = db_query("SELECT nid FROM node WHERE nid = :nid", array(':nid' => $nid));
    if ($node_id) {
      $node = node_load($nid);
      foreach ($form_state['values']['domain'] as $domain_id) {
        if ($domain_id == 0) {
          $node->domain_site = TRUE;
          $node->domains[$domain_id] = $domain_id;
        }
        else {
          $node->domain_site = FALSE;
          $node->domains[$domain_id] = $domain_id;
        }
      }
      node_save($node);
    }
  }

  drupal_set_message(
    format_plural($count,
      t('Domain access of :count node has been updated.', array(':count' => $count)),
      t('Domain access of :count nodes has been updated.', array(':count' => $count))),
    'status');
}

/**
 * Getting the list of currently active domains.
 *
 * @return array
 *   Array with key value pair of domain options.
 */
function domain_content_management_get_domain_options() {
  // From the domain module.
  $active_domains = domain_domains();
  // Setting the default value for All domains.
  $domain_options = array('0' => 'All domains');
  foreach ($active_domains as $value) {
    $domain_options[$value['domain_id']] = $value['sitename'];
  }
  return $domain_options;
}
